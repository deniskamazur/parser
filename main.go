package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/go-log/log"
	"golang.org/x/text/encoding/charmap"
)

type essay struct {
	Text   string `json:"text"`
	Rating string `json:"rating"`
	Title  string `json:"title"`
}

func win2utf(win string) string {
	b := bytes.NewBuffer([]byte(win))

	decoder := charmap.Windows1251.NewDecoder()
	reader := decoder.Reader(b)

	inBytes, err := ioutil.ReadAll(reader)
	if err != nil {
		return ""
	}

	return string(inBytes)
}

func pargePage(page string) (es essay, err error) {
	res, err := http.Get(page)
	if err != nil {
		return essay{}, err
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return essay{}, err
	}

	es.Rating = doc.Find("#hypercontext > center:nth-child(1) > span:nth-child(1)").Text()

	var text []string
	doc.Find("#hypercontext > p").Each(
		func(i int, s *goquery.Selection) {
			text = append(
				text,
				s.Text(),
			)
		},
	)

	{
		es.Text = strings.Join(text, "\n")

		b := bytes.NewBuffer([]byte(es.Text))

		decoder := charmap.Windows1251.NewDecoder()
		reader := decoder.Reader(b)

		inBytes, err := ioutil.ReadAll(reader)
		if err != nil {
			return essay{}, err
		}

		es.Text = string(inBytes)
	}

	es.Title, _ = doc.Find("#maindiv > center > table:nth-child(1) > tbody > tr > td.centerk > table > tbody > tr > td:nth-child(1) > table > tbody > tr > td > table > tbody > tr > td > div > div > div > div.t2 > h1 > span").Html()
	{
		b := bytes.NewBuffer([]byte(es.Title))

		decoder := charmap.Windows1251.NewDecoder()
		reader := decoder.Reader(b)

		inBytes, err := ioutil.ReadAll(reader)
		if err != nil {
			return essay{}, err
		}

		es.Title = string(inBytes)
	}

	return es, err
}

func parseEssayLinks(url string) (urls []string, err error) {
	res, err := http.Get(url)
	if err != nil {
		return urls, err
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return urls, err
	}

	selection := doc.Find("#maindiv > center > table:nth-child(1) > tbody > tr:nth-child(1) > td:nth-child(2) > fieldset > font > ol:nth-child(3)")

	var links []string
	selection.Find("a").Each(
		func(i int, s *goquery.Selection) {
			href, exists := s.Attr("href")
			if !exists {
				return
			}

			links = append(
				links,
				"https://www.kritika24.ru/"+href,
			)
		},
	)

	return links, nil
}

/*
func main() {
	es, err := pargePage("https://www.kritika24.ru/page.php?id=104716")
	if err != nil {
		panic(err)
	}

	file, err := os.OpenFile("result.json", os.O_WRONLY, os.ModeAppend)
	defer file.Close()
	if err != nil {
		panic(err)
	}

	if err := json.NewEncoder(file).Encode(&es); err != nil {
		panic(err)
	}
}
*/

func main() {
	base := "https://www.kritika24.ru/page.php?id=1907&top=kritik&cat=EGJe_2013&page=%d"

	var essays []essay
	for i := 1; i < 204; i++ {
		fmt.Printf("parsing page %d\n", i)

		urls, err := parseEssayLinks(fmt.Sprintf(base, i))
		if err != nil {
			log.Log("failed to parse essay links: ", err)
		}

		for _, url := range urls {
			ess, err := pargePage(url)
			if err != nil {
				log.Log("failed to parse essay: ", err)
				continue
			}

			essays = append(
				essays,
				ess,
			)
		}

		file, _ := os.Create(fmt.Sprintf("result%d.json", i))
		file, err = os.OpenFile(fmt.Sprintf("result%d.json", i), os.O_WRONLY, os.ModeAppend)
		defer file.Close()
		if err != nil {
			panic(err)
		}

		if err := json.NewEncoder(file).Encode(
			struct {
				Essays []essay `json:"essays"`
			}{Essays: essays},
		); err != nil {
			panic(err)
		}

		essays = []essay{}
	}
}
