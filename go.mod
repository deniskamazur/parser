module example.com

go 1.15

require (
	astuart.co/goq v1.0.0 // indirect
	github.com/PuerkitoBio/goquery v1.6.0
	github.com/go-log/log v0.2.0
	golang.org/x/text v0.3.0
)
